# README #

SMS Forwarding Android App

### What is this repository for? ###

* Demo app to test forwarding of incoming SMS messages to bluetooth paired device.

### How do I get set up? ###

* Use Eclipse with Android SDK installed.

### How to connect to prototype? ###

* Ensure the mbed (bluetooth module) and Android device are paired beforehand.
* Pairing pin is 1234.

### Who do I talk to? ###

* Dharmesh