package com.example.smsforwarding;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	static final String FORWARD_SMS_ACTION = "com.example.smsforwarding.MainActivity";
	
	private static final int BT_ENABLE_REQUEST = 1;
	
	// UUID for SPP (Serial Port Profile) from Android Doc
	private static final UUID MY_UUID 
		= UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");	
	
	// MAC address for prototype
	private static String address = "00:12:11:27:30:49";
	
	private BluetoothAdapter mBluetoothAdapter = null;
	private BluetoothSocket mBluetoothSocket = null;
	private OutputStream outStream = null;
	private InputStream inStream = null;
	
	// GUI objects
	private ScrollView mScrollStatus;
	private TextView mTextStatus;
	private Button mButtonBluetooth;
	private Button mButtonConnect;
	
	private BroadcastReceiver smsReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(FORWARD_SMS_ACTION)) {
				printStatus("SMS Received");
				String sender = intent.getStringExtra("sender");
				String message = intent.getStringExtra("message");
				if (isConnected()) {
					forwardSMS(sender, message);
				} else {
					printStatus("ERROR - Device not connected; SMS unforwarded");
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
			
		mScrollStatus = (ScrollView) findViewById(R.id.mScrollStatus);
		mTextStatus = (TextView) findViewById(R.id.mTextStatus);
		mButtonBluetooth = (Button) findViewById(R.id.mButtonBluetooth);
		mButtonConnect = (Button) findViewById(R.id.mButtonConnect);
		
		initialiseButtons();
		setUpReceivers();
		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	}
	
	private void setUpReceivers() {
		LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
		IntentFilter intentFilter = new IntentFilter(FORWARD_SMS_ACTION);
		bManager.registerReceiver(smsReceiver, intentFilter);
	}
	
	private void initialiseButtons() {
		mButtonBluetooth.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				turnBluetoothOn();
			}
		});
		
		mButtonConnect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				connect();
			}
		});
	}
	
	private void printStatus(String status) {
		String currText = mTextStatus.getText().toString();
		String newText = currText + getCurrentTimeStamp() + ": " + status + '\n';
		mTextStatus.setText(newText);
		scrollToBottom();
	}
	
	private void scrollToBottom() {
	    mScrollStatus.post(new Runnable() { 
	        public void run() { 
	            mScrollStatus.smoothScrollTo(0, mTextStatus.getBottom());
	        } 
	    });
	}
	
	@SuppressLint("SimpleDateFormat")
	private static String getCurrentTimeStamp() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss.SSS");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	
	private void turnBluetoothOn() {
		if (mBluetoothAdapter == null) {
			printStatus("ERROR - Bluetooth not supported");
		} else if (mBluetoothAdapter.isEnabled()) {
			printStatus("Bluetooth is already on");
		} else {
			Intent enableBluetoothIntent 
				= new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBluetoothIntent, BT_ENABLE_REQUEST);
		}
	}
	
	private boolean isConnected() {
		return (mBluetoothSocket != null && mBluetoothSocket.isConnected() 
					&& inStream != null && outStream != null);
	}
	
	private void connect() {
		if (mBluetoothAdapter.isEnabled()) {

			if (!isConnected()) {
		
				printStatus("Attempting client connect");
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
				
				try {
					mBluetoothSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
				} catch (IOException e) {
					printStatus("ERROR - Failed to create bluetooth socket");
					return;
				}
				
				mBluetoothAdapter.cancelDiscovery();
				
				printStatus("Connecting to device");
				
				
				
				try {
					mBluetoothSocket.connect();
					printStatus("Connection established");
				} catch (IOException e) {
					printStatus("ERROR - Failed to establish connection");
					try {
						mBluetoothSocket.close();
						printStatus("Bluetooth socket closed");
					} catch (IOException e2) {
						printStatus("ERROR - Failed to close bluetooth socket");
					}
					return;
				}
				
				printStatus("Setting up output stream");
				try {
					outStream = mBluetoothSocket.getOutputStream();
				} catch (IOException e) {
					printStatus("ERROR - Failed to set up output stream");
				}
				
				printStatus("Setting up input stream");
				try {
					inStream = mBluetoothSocket.getInputStream();
				} catch (IOException e ) {
					printStatus("ERROR - Failed to set up input stream");
				}
				
			} else {
				printStatus("Prototype already connected");
			}
			
		} else {
			printStatus("ERROR - Bluetooth not enabled");
		}
	}
	
	private void forwardSMS(String sender, String message) {
		//String msg = "**********" + "Sender: " + sender + "; " 
			//			+ "Message: " + message + "#";	
		String msg = "**********Testing#";
		try {
			outStream.write(msg.getBytes());
			printStatus("SMS Forwarded");
		} catch (IOException e) {
			printStatus("ERROR - Failed to forward SMS");
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == BT_ENABLE_REQUEST) {
			if (resultCode == RESULT_CANCELED) {
				printStatus("ERROR - Failed to turn bluetooth on");
			} else {
				printStatus("Bluetooth On");
			}
		}
	}
}