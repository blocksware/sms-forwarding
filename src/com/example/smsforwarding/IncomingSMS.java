package com.example.smsforwarding;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

public class IncomingSMS extends BroadcastReceiver {

	private static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(SMS_RECEIVED_ACTION)) {
			final Bundle bundle = intent.getExtras();
			try {
				if (bundle != null) {	
					final Object[] pdus = (Object[]) bundle.get("pdus");

					if (pdus.length == 0) {
						return;
					}

					SmsMessage[] messages = new SmsMessage[pdus.length];
					
					String message = getMessageBody(messages, pdus);
					String sender = messages[0].getOriginatingAddress();
					
					sendSmsBroadcast(sender, message, context);
				}
			} catch (Exception e) {
				Log.e("SmsReceiver", "Exception smsReceiver" + e);
			}	
		}
	}
	
	private String getMessageBody(SmsMessage[] messages, Object[] pdus) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < pdus.length; i++) {
			messages[i] = SmsMessage.createFromPdu( (byte[]) pdus[i] );
			sb.append(messages[i].getMessageBody());
		}
		
		return sb.toString();
	}
	
	private void sendSmsBroadcast(String sender, String message, Context context) {
		Intent smsIntent = new Intent(MainActivity.FORWARD_SMS_ACTION);
		smsIntent.putExtra("sender", sender);
		smsIntent.putExtra("message", message);
		LocalBroadcastManager.getInstance(context).sendBroadcast(smsIntent);
	}

}
